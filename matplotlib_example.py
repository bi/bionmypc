import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV files
path_to_csv_files = 'C:\\Users\\sjackson\\cernbox\\Documents'
df1 = pd.read_csv(path_to_csv_files + '\\Beam1_Beam_Intensity.csv')
df2 = pd.read_csv(path_to_csv_files + '\\Beam2_Beam_Intensity.csv')
df3 = pd.read_csv(path_to_csv_files + '\\Beam_Loss.csv')

# Plotting the data
plt.rcParams['figure.dpi'] = 300
fig = plt.figure()

ax1 = fig.add_subplot(111, label="Beam Intensity")
ax1.set_ylabel('Intensity', color="black")
ax1.yaxis.set_label_position('left')
ax1.yaxis.tick_left()

ax2 = fig.add_subplot(111, label="Beam Losses", frame_on=False)
ax2.set_ylabel('Losses', color="green")
ax2.yaxis.set_label_position('right')
ax2.yaxis.tick_right()

ax1.plot(df1, color='blue')
ax1.plot(df2, color='red')
ax2.plot(df3, color='green')

# Adding labels and title
plt.title('Data from the LHC')

# Display the plot
plt.show()
