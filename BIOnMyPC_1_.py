import audioop
import numpy as np
import pyaudio
import cv2


def make_projection(is_horizontal, image_src):
    # size of the graph
    window_size = 180
    # projection is either the sum of the inverted rows or cols
    projection = np.sum(image_src - 255, 1 if is_horizontal else 0)
    # result is a 2D array set by default to white
    result = np.empty((projection.shape[0], window_size) if is_horizontal else (
        window_size, projection.shape[0]))
    result.fill(255)  # clear to white
    # max_value will help us scale our lines
    max_value = np.max(projection)

    for line in range(projection.shape[0]):
        start_point = (window_size, line) if is_horizontal else (
            line, window_size)
        line_length = window_size - int(projection[line]*window_size/max_value)
        end_point = (line_length, line) if is_horizontal else (
            line, line_length)
        cv2.line(result, start_point, end_point, (0, 0, 0), 1)
    # return 2D image array and 1st index (may be > 1) of where max value lies
    return result, np.where(projection == max_value)[0][0]


# open first video device
video = cv2.VideoCapture(0)
# open audio device and graph a 16 bit stream with 1K points
audio = pyaudio.PyAudio()
audio_stream = audio.open(format=pyaudio.paInt16, channels=1,
                          rate=44100, input=True, frames_per_buffer=1024)

# exercise we are doing
exercise = 1
while True:
    # read the next colour imaage and convert to grayscale
    ret, image_colour = video.read()
    image_grayscale = cv2.cvtColor(image_colour,  cv2.COLOR_BGR2GRAY)

    # read a slice of sound data
    sound_data = audio_stream.read(1024)
    # here's how you can calculate the volume - We want a small value
    sound_rms = audioop.rms(sound_data, 2) / 500

    if exercise == 1:
        # Use grayscale images to get projection data
        horiz_projection_image, horiz_max = make_projection(
            True, image_grayscale)
        vert_projection_image, vert_max = make_projection(
            False, image_grayscale)
        # circle on colour_image @ location of max x,y projections in image
        centre = (horiz_max, vert_max)
        cv2.circle(image_colour, centre, 5, (128, 128, 128), 5)
        # circle on colour_image @ location of max x,y point in image
        y, x = np.unravel_index(
            np.argmax(image_grayscale), image_grayscale.shape)
        cv2.circle(image_colour, (x, y), 10, (128, 128, 128), 2)

        image_to_display = image_colour
    elif exercise == 2:
        # add the volume to the colour image to distort it
        noisy_image_colour = image_colour + int(sound_rms)
        # get projection data from a grayscale version of noisy image
        noisy_image_grayscale = cv2.cvtColor(
            noisy_image_colour,  cv2.COLOR_BGR2GRAY)
        horiz_projection_image, horiz_max = make_projection(
            True, noisy_image_grayscale)
        vert_projection_image, vert_max = make_projection(
            False, noisy_image_grayscale)

        image_to_display = noisy_image_colour
    else:
        # add the volume to the colour image to distort it using random noise
        noise = np.random.normal(
            10, int(sound_rms), image_grayscale.shape).astype(np.uint8)
        noisy_image_colour = cv2.add(
            image_colour, cv2.cvtColor(noise,  cv2.COLOR_GRAY2BGR))
        noisy_image_grayscale = cv2.cvtColor(
            noisy_image_colour,  cv2.COLOR_BGR2GRAY)
        # get projection data from a grayscale version of noisy image
        horiz_projection_image, horiz_max = make_projection(
            True, noisy_image_grayscale)
        vert_projection_image, vert_max = make_projection(
            False, noisy_image_grayscale)

        image_to_display = noisy_image_colour

    # display the image and projections
    cv2.putText(image_to_display, "Exercise #"+str(exercise), (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (128, 128, 45))
    cv2.imshow('Input', image_to_display)
    cv2.imshow('Projection H', horiz_projection_image)
    cv2.imshow('Projection V', vert_projection_image)

    key_pressed = cv2.waitKey(1)
    if key_pressed == 27:
        break
    # convert to a number matching the example
    key_pressed = key_pressed - ord('1') + 1
    if 1 <= key_pressed <= 3:
        exercise = key_pressed

audio_stream.stop_stream()
audio_stream.close()
audio.terminate()

video.release()
cv2.destroyAllWindows()
